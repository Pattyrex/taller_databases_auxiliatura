CREATE TABLE cliente (
  id_cliente SERIAL PRIMARY KEY,
  id_tienda INT,
  nombre VARCHAR(30),
  apellido VARCHAR(30),
  correo_electronico VARCHAR(50),
  id_direccion INT,
  activo BOOLEAN,
  CONSTRAINT fk_tienda_cliente FOREIGN KEY (id_tienda) REFERENCES tienda(id_tienda),
  CONSTRAINT fk_direccion_cliente FOREIGN KEY (id_direccion) REFERENCES direccion(id_direccion)
);

CREATE TABLE actor (
  id_actor SERIAL PRIMARY KEY,
  nombre VARCHAR(30),
  apellido VARCHAR(30),
  ultima_actualizacion TIMESTAMP
);

CREATE TABLE categoria (
  id_categoria SERIAL PRIMARY KEY,
  nombre VARCHAR(30),
  ultima_actualizacion TIMESTAMP
);

CREATE TABLE pelicula (
  id_pelicula SERIAL PRIMARY KEY,
  titulo VARCHAR(100),
  descripcion TEXT,
  año_estreno INT,
  id_idioma INT,
  duracion_alquiler INT,
  tarifa_alquiler FLOAT,
  duracion INT,
  costo_reemplazo FLOAT,
  clasificacion VARCHAR(10),
  ultima_actualizacion TIMESTAMP,
  caracteristicas_especiales TEXT[],
  texto_completo TEXT,
  CONSTRAINT fk_idioma_pelicula FOREIGN KEY (id_idioma) REFERENCES idioma(id_idioma)
);

CREATE TABLE actor_pelicula (
  id_actor INT,
  id_pelicula INT,
  ultima_actualizacion TIMESTAMP,
  CONSTRAINT fk_pelicula_actor FOREIGN KEY (id_pelicula) REFERENCES pelicula(id_pelicula),
  CONSTRAINT fk_actor_pelicula FOREIGN KEY (id_actor) REFERENCES actor(id_actor) 
);

CREATE TABLE categoria_pelicula (
  id_pelicula INT,
  id_categoria INT,
  ultima_actualizacion TIMESTAMP,
  CONSTRAINT pk_categoria_pelicula PRIMARY KEY (id_pelicula, id_categoria),
  CONSTRAINT fk_pelicula_categoria FOREIGN KEY (id_pelicula) REFERENCES pelicula(id_pelicula),
  CONSTRAINT fk_categoria_pelicula FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria)
);

CREATE TABLE direccion (
  id_direccion SERIAL PRIMARY KEY,
  direccion VARCHAR(100),
  direccion2 VARCHAR(100),
  distrito VARCHAR(50),
  id_ciudad INT,
  codigo_postal VARCHAR(10),
  telefono VARCHAR(10),
  ultima_actualizacion TIMESTAMP,
  CONSTRAINT fk_ciudad_direccion FOREIGN KEY (id_ciudad) REFERENCES ciudad(id_ciudad)
);

CREATE TABLE ciudad (
  id_ciudad SERIAL PRIMARY KEY,
  nombre VARCHAR(50),
  id_pais INT,
  ultima_actualizacion TIMESTAMP,
  CONSTRAINT fk_pais_ciudad FOREIGN KEY (id_pais) REFERENCES pais(id_pais) 
);

CREATE TABLE pais (
  id_pais SERIAL PRIMARY KEY,
  nombre VARCHAR(50),
  ultima_actualizacion TIMESTAMP
);

CREATE TABLE inventario (
  id_inventario SERIAL PRIMARY KEY,
  id_pelicula INT,
  id_tienda INT,
  ultima_actualizacion TIMESTAMP,
  CONSTRAINT fk_pelicula_inventario FOREIGN KEY (id_pelicula) REFERENCES pelicula(id_pelicula),
  CONSTRAINT fk_tienda_inventario FOREIGN KEY (id_tienda) REFERENCES tienda(id_tienda)
);

CREATE TABLE idioma (
  id_idioma SERIAL PRIMARY KEY,
  nombre VARCHAR(50),
  ultima_actualizacion TIMESTAMP
);

CREATE TABLE pago (
  id_pago SERIAL PRIMARY KEY,
  id_cliente INT,
  id_personal INT,
  id_renta INT,
  monto FLOAT,
  fecha_pago TIMESTAMP,
  CONSTRAINT fk_cliente_pago FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente),
  CONSTRAINT fk_personal_pago FOREIGN KEY (id_personal) REFERENCES personal(id_personal),
  CONSTRAINT fk_renta_pago FOREIGN KEY (id_renta) REFERENCES renta(id_renta)
);

CREATE TABLE renta (
  id_renta SERIAL PRIMARY KEY,
  fecha_renta TIMESTAMP,
  id_inventario INT,
  id_cliente INT,
  fecha_devolucion TIMESTAMP,
  id_personal INT,
  ultima_actualizacion TIMESTAMP,
  CONSTRAINT fk_inventario_renta FOREIGN KEY (id_inventario) REFERENCES inventario(id_inventario),
  CONSTRAINT fk_cliente_renta FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente),
  CONSTRAINT fk_personal_renta FOREIGN KEY (id_personal) REFERENCES personal(id_personal)
);

CREATE TABLE personal (
  id_personal SERIAL PRIMARY KEY,
  nombre VARCHAR(50),
  apellido VARCHAR(50),
  id_direccion INT,
  correo_electronico VARCHAR(100),
  id_tienda INT,
  activo BOOLEAN,
  nombre_usuario VARCHAR(50),
  contrasena VARCHAR(50),
  ultima_actualizacion TIMESTAMP,
  CONSTRAINT fk_direccion_personal FOREIGN KEY (id_direccion) REFERENCES direccion(id_direccion),
  CONSTRAINT fk_tienda_personal FOREIGN KEY (id_tienda) REFERENCES tienda(id_tienda) // id?tienda hay que declararlo clave foránea despues, no ahora xd
);

CREATE TABLE tienda (
  id_tienda SERIAL PRIMARY KEY,
  id_personal INT,
  id_direccion INT,
  ultima_actualizacion TIMESTAMP,
  CONSTRAINT fk_personal_tienda FOREIGN KEY (id_personal) REFERENCES personal(id_personal),
  CONSTRAINT fk_direccion_tienda FOREIGN KEY (id_direccion) REFERENCES direccion(id_direccion)
);
