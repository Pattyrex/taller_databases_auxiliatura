insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (1, 'Batman/Superman Movie, The', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 2010, '12/7/2023', '15:59', 760.1, 180, 379.49, 'Action|Adventure|Animation|Children|Fantasy|Sci-Fi', '1/9/2024', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (2, 'Cure', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 2006, '6/9/2023', '11:15', 790.91, 103, 773.96, 'Crime|Horror|Thriller', '3/1/2024', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (3, 'Mary', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 1998, '8/27/2023', '14:21', 196.14, 76, 499.04, 'Drama|Thriller', '4/10/2024', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (4, 'The Very Thought of You', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 1998, '12/1/2023', '14:52', 986.58, 114, 426.55, 'Drama|Romance', '4/25/2024', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (5, 'In Her Shoes', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 1992, '4/5/2024', '3:12', 422.46, 96, 66.86, 'Comedy|Drama', '3/13/2024', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (6, 'Dead of the Nite', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', 2005, '11/15/2023', '0:43', 231.54, 122, 186.95, 'Horror', '6/7/2023', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (7, 'Wild Strawberries (Smultronstället)', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 2002, '8/27/2023', '12:18', 174.6, 170, 27.67, 'Drama', '4/15/2024', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (8, 'World''s End, The', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 1992, '10/21/2023', '10:46', 956.46, 164, 83.55, 'Action|Comedy|Sci-Fi', '4/19/2024', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (9, 'Jessie James Meets Frankenstein''s Daughter', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 1992, '8/30/2023', '1:49', 704.15, 161, 825.68, 'Sci-Fi|Western', '2/15/2024', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (10, 'Bed & Breakfast: Love is a Happy Accident (Bed & Breakfast)', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 2004, '8/26/2023', '10:40', 867.39, 74, 285.0, 'Comedy|Romance', '8/1/2023', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (11, 'Back to Bataan', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 1993, '2/27/2024', '15:50', 747.94, 100, 560.65, 'Drama|War', '3/29/2024', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (12, 'Manhattan Melodrama', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 2000, '3/28/2024', '9:45', 229.08, 166, 22.75, 'Crime|Drama|Romance', '9/30/2023', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (13, 'Johnny Belinda', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 1999, '1/21/2024', '2:51', 405.36, 93, 206.95, 'Drama', '12/18/2023', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (14, 'Bluebeard (Landru)', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 1997, '2/20/2024', '23:19', 401.57, 90, 759.63, 'Drama', '1/6/2024', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (15, 'Aaja Nachle', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 1993, '2/19/2024', '4:56', 921.38, 71, 695.88, 'Comedy|Musical|Romance', '10/20/2023', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (16, 'Prince of Jutland (a.k.a. Royal Deceit)', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 1995, '8/17/2023', '12:50', 398.86, 167, 362.59, 'Drama', '1/11/2024', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (17, 'Bernie', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 2012, '11/22/2023', '3:48', 612.35, 76, 131.47, 'Comedy|Crime|Drama', '9/20/2023', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (18, 'Patent Leather Kid, The', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.', 2001, '3/17/2024', '9:01', 890.3, 172, 157.32, 'Drama', '4/4/2024', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (19, 'Inn of Evil (Inochi bô ni furô)', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 1993, '3/18/2024', '1:20', 772.09, 174, 229.14, 'Crime|Drama', '9/4/2023', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (20, 'Woody Allen: A Documentary', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', 2006, '9/21/2023', '16:11', 76.81, 151, 757.4, 'Documentary', '1/25/2024', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (21, 'Change of Habit', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 2008, '7/27/2023', '7:38', 914.04, 133, 298.62, 'Drama', '12/16/2023', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (22, 'Nowhere Boy', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 2004, '1/16/2024', '3:38', 795.13, 140, 117.39, 'Drama', '8/3/2023', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (23, 'Act of Killing, The', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 2003, '3/31/2024', '12:32', 268.87, 65, 476.44, 'Documentary', '5/27/2023', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (24, 'Ebola Syndrome, The (Yi boh laai beng duk)', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', 2012, '7/30/2023', '23:37', 239.92, 91, 838.74, 'Comedy|Horror', '8/10/2023', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (25, 'Year of the Dog', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 1996, '3/4/2024', '7:28', 603.26, 95, 490.83, 'Comedy|Drama|Romance', '3/19/2024', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (26, 'Thunderbolt (Pik lik feng)', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 1987, '2/7/2024', '23:01', 497.21, 64, 669.22, 'Action|Crime', '9/13/2023', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (27, 'Descendant of the Sun (Ri jie)', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 1995, '12/7/2023', '10:08', 407.87, 158, 695.76, 'Action|Adventure|Fantasy|Sci-Fi', '7/5/2023', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (28, 'Cameraman, The', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', 1984, '9/29/2023', '3:50', 800.51, 91, 633.3, 'Comedy|Drama|Romance', '3/23/2024', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (29, 'Running Time', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 2005, '2/11/2024', '6:49', 983.21, 96, 815.56, 'Crime|Thriller', '7/17/2023', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (30, 'Shadows', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 1988, '5/29/2023', '2:22', 539.61, 123, 28.47, 'Drama', '6/11/2023', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (31, '100 Years at the Movies', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 2001, '5/28/2023', '17:18', 419.65, 90, 906.6, 'Documentary', '5/14/2023', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (32, 'Mao''s Last Dancer', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 1999, '6/15/2023', '12:30', 756.36, 131, 356.48, 'Drama', '2/22/2024', 'Fusce consequat. Nulla nisl. Nunc nisl.', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (33, 'Big Wedding, The', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 2008, '6/18/2023', '1:45', 895.81, 80, 174.0, 'Comedy', '2/10/2024', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (34, '7 Days (Les 7 jours du talion)', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 1994, '9/22/2023', '1:27', 198.86, 121, 411.82, 'Thriller', '4/21/2024', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (35, 'Dangerous', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 2010, '12/11/2023', '11:23', 821.62, 101, 920.24, 'Drama', '12/17/2023', 'In congue. Etiam justo. Etiam pretium iaculis justo.', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (36, 'Fierce People', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 1991, '6/7/2023', '21:55', 659.12, 104, 997.6, 'Drama', '11/22/2023', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (37, 'Last Angry Man, The', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 2006, '4/26/2024', '18:00', 310.64, 179, 670.73, 'Drama', '9/3/2023', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (38, 'Truth in 24', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 2009, '3/18/2024', '21:19', 705.12, 172, 724.99, 'Documentary', '2/29/2024', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (39, 'Thirst (Pyaasa)', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 1998, '4/23/2024', '22:58', 957.72, 118, 24.46, 'Drama|Musical|Romance', '9/29/2023', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (40, 'Test Pilot', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 1991, '1/14/2024', '5:00', 362.33, 132, 202.14, 'Drama|Romance', '8/14/2023', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (41, 'Daydreams', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 2003, '12/2/2023', '12:52', 834.58, 160, 38.2, 'Comedy', '7/8/2023', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (42, 'Serial Mom', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 2003, '7/26/2023', '1:48', 369.17, 76, 716.5, 'Comedy|Crime|Horror', '1/18/2024', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (43, 'Planet of the Vampires (Terrore nello spazio)', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 2009, '11/21/2023', '17:05', 474.9, 91, 842.64, 'Horror|Sci-Fi', '9/13/2023', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (44, 'Garfield''s Halloween Adventure', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 2008, '6/23/2023', '16:55', 419.64, 116, 430.18, 'Animation|Children', '3/29/2024', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (45, 'Dogs of War, The', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 1997, '3/16/2024', '21:42', 924.18, 174, 199.77, 'Drama|War', '4/13/2024', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (46, 'Late George Apley, The', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 1992, '12/24/2023', '11:20', 832.52, 64, 932.04, 'Comedy', '8/22/2023', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (47, 'Man Made Monster', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 2011, '8/26/2023', '20:29', 829.29, 142, 849.2, 'Drama|Horror|Sci-Fi|Thriller', '9/26/2023', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (48, 'Different for Girls', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 1994, '12/9/2023', '8:36', 737.26, 98, 306.9, 'Comedy', '5/31/2023', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (49, 'Big Street, The', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 1985, '1/24/2024', '14:29', 66.12, 98, 494.6, 'Drama|Romance', '1/2/2024', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (50, 'Bamako', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 2009, '2/13/2024', '6:24', 25.6, 84, 502.72, 'Drama', '5/12/2023', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (51, 'Sawdust and Tinsel (Gycklarnas afton)', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 1990, '5/29/2023', '5:14', 393.29, 146, 15.74, 'Drama', '1/29/2024', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (52, 'Nowhere to Hide (Injeong sajeong bol geot eobtda)', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 2005, '11/12/2023', '21:48', 433.26, 167, 743.6, 'Action|Comedy|Crime|Thriller', '7/19/2023', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (53, 'Big Fish', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 1999, '1/7/2024', '0:52', 155.84, 145, 845.7, 'Drama|Fantasy|Romance', '11/15/2023', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (54, 'Conflict', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 1959, '3/18/2024', '22:20', 754.27, 162, 326.97, 'Drama|Film-Noir', '12/28/2023', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (55, 'Little Brother, Big Trouble: A Christmas Adventure (Niko 2: Lentäjäveljekset)', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', 1999, '2/4/2024', '9:36', 209.09, 124, 661.07, 'Adventure|Animation|Children|Comedy', '12/24/2023', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (56, 'Milwaukee, Minnesota', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 2000, '11/27/2023', '17:09', 847.61, 108, 320.61, 'Drama', '8/16/2023', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (57, 'Passion in the Desert', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 2006, '4/12/2024', '5:41', 781.71, 111, 546.5, 'Adventure|Drama', '10/13/2023', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (58, 'Old Dogs', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 2009, '12/28/2023', '2:42', 293.34, 93, 411.8, 'Comedy', '7/15/2023', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (59, 'Sky High', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 1985, '5/24/2023', '8:21', 380.53, 128, 779.33, 'Action|Adventure|Children|Comedy', '7/7/2023', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (60, 'Bugs Bunny / Road Runner Movie, The (a.k.a. The Great American Chase)', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 2008, '8/18/2023', '13:43', 532.85, 69, 62.59, 'Animation|Children|Comedy', '11/13/2023', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (61, 'Broadway Serenade', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', 1986, '2/21/2024', '21:17', 651.46, 145, 235.2, 'Drama|Musical|Romance', '6/2/2023', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (62, 'The Rag Man', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 1997, '3/4/2024', '16:48', 757.49, 177, 709.26, 'Comedy|Drama', '11/10/2023', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (63, 'Greatest Love, The (Europa ''51)', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 2012, '8/20/2023', '20:08', 362.64, 68, 693.06, 'Drama', '5/19/2023', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (64, 'Destroy All Monsters (Kaijû sôshingeki)', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 2005, '10/14/2023', '23:44', 903.08, 134, 283.57, 'Action|Sci-Fi|Thriller', '1/27/2024', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (65, 'Säg att du älskar mig', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 1997, '11/15/2023', '3:53', 211.8, 112, 756.11, 'Drama', '11/21/2023', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (66, 'Ninjas vs. Zombies', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 1994, '12/30/2023', '7:14', 53.16, 69, 269.97, 'Action|Comedy|Fantasy|Horror', '8/12/2023', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (67, 'Turbulence', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 2007, '8/5/2023', '15:46', 976.66, 126, 546.43, 'Action|Thriller', '2/15/2024', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (68, 'Whistle Blower, The', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 2008, '6/30/2023', '18:14', 120.33, 75, 812.41, 'Thriller', '5/1/2023', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (69, 'Breakout', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 1987, '7/27/2023', '13:00', 579.37, 72, 123.31, 'Action|Adventure', '7/25/2023', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (70, 'Hoodwinked!', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 2005, '6/8/2023', '5:13', 552.91, 152, 8.9, 'Animation|Children|Comedy', '5/23/2023', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (71, 'i hate myself :)', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 1963, '4/21/2024', '17:12', 690.18, 162, 700.32, 'Comedy|Documentary|Drama', '1/5/2024', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (72, 'Click', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 2004, '3/13/2024', '5:28', 268.2, 121, 476.88, 'Adventure|Comedy|Drama|Fantasy|Romance', '9/8/2023', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (73, 'Woman of Antwerp', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 2004, '12/25/2023', '12:33', 891.34, 63, 914.66, 'Drama', '8/16/2023', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (74, 'Curse of the Puppet Master (Puppet Master 6: The Curse)', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 2012, '7/23/2023', '5:17', 391.86, 163, 974.33, 'Horror|Sci-Fi|Thriller', '2/20/2024', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (75, 'Shaolin Kung Fu Mystagogue (Da mo mi zong)', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 1994, '4/26/2024', '12:11', 47.66, 117, 627.33, 'Action|Drama', '5/29/2023', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (76, 'Tully', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 1994, '3/14/2024', '7:59', 942.03, 179, 918.4, 'Drama', '8/13/2023', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (77, 'Farewell to Matyora', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 1998, '6/19/2023', '16:26', 975.46, 68, 883.34, 'Drama', '8/2/2023', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (78, 'Ju Dou', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 1985, '5/11/2023', '6:52', 549.2, 89, 478.78, 'Drama', '11/21/2023', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (79, 'Good, the Bad, the Weird, The (Joheunnom nabbeunnom isanghannom)', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 2005, '4/23/2024', '7:38', 936.49, 118, 650.13, 'Action|Adventure|Comedy|Western', '8/16/2023', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (80, 'Varg Veum - Bitter Flowers (Varg Veum - Bitre Blomster)', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 2007, '7/29/2023', '7:06', 63.38, 64, 543.63, 'Crime|Drama|Thriller', '4/30/2023', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (81, 'Black and White', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 2009, '5/6/2023', '22:26', 94.5, 90, 377.99, 'Drama', '10/8/2023', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (82, 'Lady is Willing, The', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 1999, '12/26/2023', '13:22', 209.49, 113, 499.11, 'Comedy|Drama|Romance', '12/25/2023', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (83, 'Piano in a Factory, The (Gang de qin)', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', 1993, '3/27/2024', '11:03', 464.79, 135, 68.08, 'Comedy|Drama|Musical', '6/26/2023', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (84, 'Criminal Lovers', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 2013, '2/3/2024', '17:30', 656.64, 152, 901.96, 'Crime|Drama|Romance|Thriller', '2/4/2024', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (85, 'Ice Soldiers', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 2012, '4/11/2024', '8:56', 830.89, 140, 208.42, 'Action|Sci-Fi', '2/6/2024', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (86, '24 City (Er shi si cheng ji)', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 2005, '5/15/2023', '19:37', 236.08, 143, 831.93, 'Drama', '11/19/2023', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (87, 'The Cyclone', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 1961, '5/26/2023', '5:08', 133.04, 154, 420.18, 'Comedy|Romance', '7/27/2023', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (88, 'Now and Then', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 2011, '2/5/2024', '3:56', 216.72, 169, 22.45, 'Children|Drama', '3/5/2024', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (89, 'Last Chance: Diary of Comedians, The (Bokutachi no koukan nikki)', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 1964, '7/13/2023', '3:24', 765.45, 156, 229.45, 'Comedy|Drama', '4/11/2024', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (90, 'Far Out Man', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 1997, '7/29/2023', '4:39', 612.51, 100, 241.83, 'Comedy', '12/21/2023', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (91, 'I Am Number Four', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 1992, '11/13/2023', '3:47', 629.15, 89, 923.51, 'Action|Sci-Fi|Thriller|IMAX', '4/2/2024', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (92, 'Favela Rising', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 2005, '5/17/2023', '15:02', 314.86, 120, 659.4, 'Documentary', '7/8/2023', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (93, 'Water Horse: Legend of the Deep, The', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 2011, '6/21/2023', '23:27', 522.71, 81, 961.79, 'Adventure|Children|Fantasy', '6/3/2023', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (94, 'Shaggy Dog, The', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 2012, '1/15/2024', '1:49', 97.97, 97, 759.18, 'Adventure|Children|Comedy|Fantasy', '1/5/2024', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (95, 'Pieta', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 2004, '1/28/2024', '8:03', 859.91, 153, 486.19, 'Drama', '12/10/2023', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (96, 'Life and Times of Allen Ginsberg, The', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 2006, '6/27/2023', '23:54', 432.07, 66, 297.49, 'Documentary', '1/9/2024', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (97, 'Highway to Hell', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 1988, '8/18/2023', '22:38', 78.22, 179, 655.9, 'Action|Comedy|Fantasy|Horror', '2/20/2024', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (98, 'Six Degrees of Separation', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 2010, '10/6/2023', '21:37', 408.72, 62, 713.25, 'Drama', '4/13/2024', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (99, 'Ascent of Money, The', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 1987, '10/15/2023', '7:10', 606.38, 136, 623.59, 'Documentary', '3/10/2024', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.');
insert into pelicula (id_pelicula, titulo, descripcion, año_estreno, id_idioma, duracion_alquiler, tarifa_alquiler, duracion, costo_reemplazo, clasificacion, ultima_actualizacion, caracteristicas_especiales, texto_completo) values (100, 'Saints and Soldiers: The Void', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 1997, '4/21/2024', '14:05', 758.9, 89, 755.74, 'Action|Drama|War', '12/15/2023', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.');
